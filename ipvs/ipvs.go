package ipvs

import (
	"bufio"
	"encoding/json"
	"gitlab.com/sergeyignatov/goipvs/hc"
	"io/ioutil"

	"fmt"
	"gitlab.com/sergeyignatov/goipvs/api"
	"os"
	"strings"
)

var (
	ipVSFile string = "/proc/net/ip_vs"
)

type IPVS struct {
	virtualServers map[api.VirtualServerKey]*api.VirtualServer
	hc             *hc.HealthChecker
}

func (i *IPVS) Apply(file string) (err error) {

	if err = i.parseIPVS(ipVSFile); err != nil {
		return
	}

	data, err := ioutil.ReadFile(file)
	if err != nil {
		return
	}
	vs := map[api.VirtualServerKey]api.VirtualServer{}
	if err = json.Unmarshal(data, &vs); err != nil {
		return
	}
	fmt.Printf("%#v\n", vs)
	//add and update

	for k, v := range vs {
		if _, ok := i.virtualServers[k]; !ok { //add
			if err = i.AddVS(v); err != nil {
				return
			}
		} else {
			if err = i.UpdateVS(k, v); err != nil {
				return
			}
			for k1, v1 := range v.RealServers {
				if _, ok := i.virtualServers[k].RealServers[k1]; !ok { //add new
					if err = i.AddRS(k, v1); err != nil {
						return
					}
				} else { //update
					if err = i.UpdateRS(k, v1); err != nil {
						return
					}
				}
			}
		}
	}

	// delete
	for k, v := range i.virtualServers {
		if _, ok := vs[k]; !ok {
			fmt.Println("delete virtual server", k)
			if err = i.DeleteVS(k); err != nil {
				return
			}
		} else {
			for k1 := range v.RealServers {
				if _, ok := vs[k].RealServers[k1]; !ok {
					fmt.Println("delete real server", k1)
					if err = i.DeleteRS(k, k1); err != nil {
						return
					}
				}
			}
		}
	}

	return
}
func (i *IPVS) parseIPVS(file string) (err error) {
	fd, err := os.Open(file)
	if err != nil {
		return
	}
	defer fd.Close()
	scanner := bufio.NewScanner(fd)
	i.virtualServers = make(map[api.VirtualServerKey]*api.VirtualServer)
	isService := false
	var lastKey string
	for scanner.Scan() {
		line := scanner.Text()

		spl := strings.Fields(line)
		if len(spl) == 3 { //service
			isService = true
			proto := api.NewProtocol(spl[0])
			if proto == api.Unknown {
				return fmt.Errorf("cannot parse protocol")
			}
			vs, err := api.NewVSHex(spl[1], proto, spl[2])
			if err != nil {
				return fmt.Errorf("cannot parse %s", line)
			}

			i.virtualServers[vs.String()] = &vs
			lastKey = vs.String()
		} else if isService && len(spl) == 6 { //real server
			// -> 192.168.10.1:80              Masq    1      0          0

			rs, err := api.NewRSHex(spl[1], spl[2], spl[3])
			if err != nil {
				return fmt.Errorf("cannot parse %s", line)
			}
			i.virtualServers[lastKey].AddRS(rs)
		}

	}

	return
}

func (i *IPVS) DeleteVS(key api.VirtualServerKey) (err error) {
	return
}

func (i *IPVS) DeleteRS(key api.VirtualServerKey, rs api.RealServerKey) (err error) {
	return
}
func (i *IPVS) AddRS(key api.VirtualServerKey, rs api.RealServer) (err error) {
	return
}

func (i *IPVS) AddVS(vs api.VirtualServer) (err error) {
	return
}
func (i *IPVS) UpdateRS(key api.VirtualServerKey, rs api.RealServer) (err error) {
	return
}

func (i *IPVS) UpdateVS(key api.VirtualServerKey, vs api.VirtualServer) (err error) {
	return
}
