package api

import (
	"fmt"
	"strings"
)

type Protocol int

const (
	Unknown Protocol = 0
	TCP     Protocol = 6
	UDP     Protocol = 17
)

func (p *Protocol) UnmarshalJSON(data []byte) (err error) {
	data = data[1 : len(data)-1]
	switch t := string(data); t {
	case "tcp":
		*p = TCP
	case "udp":
		*p = UDP
	default:
		return fmt.Errorf("unknown protocol %s", t)
	}
	return
}

func (p Protocol) String() string {
	switch p {
	case TCP:
		return "tcp"
	case UDP:
		return "udp"
	default:
		return "unknown"
	}
	return "unknown"
}

func NewProtocol(s string) Protocol {
	switch strings.ToLower(s) {
	case "tcp":
		return TCP
	case "udp":
		return UDP
	}
	return Unknown
}
