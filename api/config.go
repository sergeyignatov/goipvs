package api

type JsonConfig struct {
	VirtualServers map[VirtualServerKey]JVirtualServer
}

type JVirtualServer struct {
	LBAlgo      string                        `json:"lb_algo"`
	LBKind      string                        `json:"lb_kind"`
	RealServers map[RealServerKey]JRealServer `json:"real_servers"`
}

type JRealServer struct {
	Weight int   `json:"Weight"`
	Pulse  Pulse `json:"HealthCheck"`
}
