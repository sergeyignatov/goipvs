package api

import (
	"encoding/binary"
	"fmt"
	"net"
	"strconv"
	"strings"
)

type IPPort struct {
	ip   net.IP
	port int
}

func (i IPPort) String() string {
	if i.IsIpv6() {
		return fmt.Sprintf("[%s]:%d", i.ip, i.port)
	}
	return fmt.Sprintf("%s:%d", i.ip, i.port)
}

func (i *IPPort) UnmarshalJSON(data []byte) (err error) {
	data = data[1 : len(data)-1]
	tcpAddr, err := net.ResolveTCPAddr("tcp", string(data))
	if err != nil {
		fmt.Println("err", err)
		return
	}
	i.port = tcpAddr.Port
	i.ip = tcpAddr.IP
	return
}

func NewIPPort(ip net.IP, port int) IPPort {
	return IPPort{
		ip:   ip,
		port: port,
	}
}
func NewIPPortHexString(ipport string) (i IPPort, err error) {
	idx1 := strings.Index(ipport, "[")
	var (
		ip     net.IP
		port64 int64
	)
	if idx1 != -1 { //ipv6
		idx2 := strings.Index(ipport, "]")
		if ip = net.ParseIP(ipport[idx1+1 : idx2]); ip == nil {
			err = fmt.Errorf("unable parse %s", ipport[idx1+1:idx2])
			return
		}
		idx3 := strings.LastIndex(ipport, ":")
		sport := ipport[idx3+1:]
		if port64, err = strconv.ParseInt(sport, 16, 32); err != nil {
			return
		}
	} else {
		idx3 := strings.LastIndex(ipport, ":")
		sport := ipport[idx3+1:]
		if port64, err = strconv.ParseInt(sport, 16, 32); err != nil {
			return
		}

		var uip uint64
		if uip, err = strconv.ParseUint(ipport[:idx3], 16, 32); err != nil {
			return
		}
		ip = make(net.IP, 4)
		binary.BigEndian.PutUint32(ip, uint32(uip))
	}
	if port64 > 65535 || port64 <= 0 {
		err = fmt.Errorf("port validation error")
		return
	}
	i = IPPort{
		ip:   ip,
		port: int(port64),
	}
	return
}

func (i IPPort) IsIpv6() bool {
	if x := i.ip.To4(); x == nil {
		return true
	}
	return false
}
