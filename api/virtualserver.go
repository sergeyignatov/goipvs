package api

import (
	"fmt"
	"net"
)

type VirtualServerKey = string

type VirtualServer struct {
	IPPort      IPPort                       `json:"ip_port"`
	Proto       Protocol                     `json:"protocol"`
	LBAlgo      string                       `json:"lb_algo"` //wlc, wrr, rr
	RealServers map[RealServerKey]RealServer `json:"real_servers"`
}

func (vs VirtualServer) String() VirtualServerKey {
	return fmt.Sprintf("%s:%s", vs.Proto, vs.IPPort)
}

func NewVS(ip net.IP, port int, protocol Protocol, algo string) VirtualServer {
	vs := VirtualServer{
		IPPort:      NewIPPort(ip, port),
		Proto:       protocol,
		LBAlgo:      algo,
		RealServers: map[RealServerKey]RealServer{},
	}
	return vs
}

func NewVSHex(sipport string, proto Protocol, algo string) (vs VirtualServer, err error) {
	ipport, err := NewIPPortHexString(sipport)
	if err != nil {
		return
	}
	vs = NewVS(ipport.ip, ipport.port, proto, algo)
	return
}

func (vs *VirtualServer) AddRS(rs RealServer) {
	vs.RealServers[rs.String()] = rs
}

/*func (vs VirtualServer) RealServers() map[RealServerKey]RealServer {
	return vs.RealServers
}*/
