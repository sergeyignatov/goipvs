package api

import (
	"container/list"
	"fmt"
	"strconv"
)

type RealServerKey = string

type RealServer struct {
	IPPort        IPPort `json:"ip_port"`
	LBKind        string `json:"lb_kind"` //nat, tun, dr
	Weight        uint16 `json:"weight"`
	runtimeWeight uint16
	status        bool
	elem          *list.Element
	HealthCheck   Pulse `json:"health_check"`
}

type CheckProto int

const (
	CheckTCP CheckProto = 1
	HTTP     CheckProto = 2
	HTTPS    CheckProto = 3
)

type Pulse struct {
	proto       CheckProto
	Uri         string `json:"uri"`
	ProtoString string `json:"Proto"`
	Url         string `json:"url"`
}

type Status struct {
	Key    RealServerKey
	Status bool
}

func (rs RealServer) String() RealServerKey {
	return rs.IPPort.String()
}

func NewRSHex(hip string, lbKind string, hexWeight string) (rs RealServer, err error) {
	ipport, err := NewIPPortHexString(hip)
	if err != nil {
		return
	}
	weight64, err := strconv.ParseUint(hexWeight, 16, 32)
	if err != nil {
		return
	}
	rs = RealServer{
		IPPort:        ipport,
		LBKind:        lbKind,
		Weight:        uint16(weight64),
		runtimeWeight: uint16(weight64),
	}
	return
}

func (rs RealServer) Elem() *list.Element {
	return rs.elem
}
func (rs *RealServer) SetElem(el *list.Element) {
	rs.elem = el
}

func (rs *RealServer) CheckURL() string {
	if len(rs.HealthCheck.Url) > 0 {
		return rs.HealthCheck.Url
	}
	switch rs.HealthCheck.proto {
	case HTTPS:
		return fmt.Sprintf("https://%s%s", rs.String(), rs.HealthCheck.Uri)
	case HTTP:
		return fmt.Sprintf("http://%s%s", rs.String(), rs.HealthCheck.Uri)
	default:
		return fmt.Sprintf("tcp://%s", rs.String())
	}
}
