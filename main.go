package main

import (
	"flag"
	"fmt"
	"gitlab.com/sergeyignatov/goipvs/ipvs"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func run() (err error) {

	config := flag.String("config", "config.json", "xxx")
	flag.Parse()
	i := ipvs.IPVS{}
	if err = i.Apply(*config); err != nil {
		return
	}

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM)

	for {
		select {
		case t := <-sigChan:
			switch t {
			case syscall.SIGHUP:
				if err = i.Apply(*config); err != nil {
					fmt.Println("err")
				}
			default:
				return
			}
		}
	}

	return
}

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}
