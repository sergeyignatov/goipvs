package hc

import (
	"container/list"
	"gitlab.com/sergeyignatov/goipvs/api"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"sync"
	"time"
)

var netTransport = &http.Transport{
	Dial: (&net.Dialer{
		Timeout: 1 * time.Second,
	}).Dial,
	TLSHandshakeTimeout: 1 * time.Second,
}

type HealthChecker struct {
	realServers map[api.RealServerKey]*api.RealServer
	quitMap     map[api.RealServerKey]chan bool
	evict       *list.List
	lock        sync.RWMutex
	Statuses    chan api.Status
	netClient   *http.Client
}
type Element struct {
	key api.RealServerKey
	ts  time.Time
}

func New() *HealthChecker {
	hc := HealthChecker{
		realServers: make(map[api.RealServerKey]*api.RealServer),
		quitMap:     make(map[api.RealServerKey]chan bool),
		evict:       list.New(),
	}
	hc.netClient = &http.Client{
		Timeout:   time.Second * 1,
		Transport: netTransport,
	}
	go func() {
		for {
			hc.eviction()
			time.Sleep(5 * time.Second)
		}
	}()
	return &hc
}
func (hc *HealthChecker) Add(r api.RealServer) {
	hc.lock.Lock()
	defer hc.lock.Unlock()
	hc.realServers[r.String()] = &r
	hc.quitMap[r.String()] = hc.checker(r)
}

func (hc *HealthChecker) Ping(r api.RealServer) {
	if s, ok := hc.realServers[r.String()]; ok {
		hc.evict.MoveToFront(s.Elem())
	} else {
		el := hc.evict.PushFront(Element{key: r.String(), ts: time.Now()})
		r.SetElem(el)
		hc.Add(r)
	}
}

func (hc *HealthChecker) eviction() {
	hc.lock.Lock()
	defer hc.lock.Unlock()
	if hc.evict.Len() == 0 {
		return
	}
	el := hc.evict.Back()
	var t Element
	for {
		if el == nil {
			break
		}
		t = el.Value.(Element)
		if time.Now().Sub(t.ts).Seconds() > 60 {
			el = el.Prev()
			if r, ok := hc.realServers[t.key]; ok {
				hc.evict.Remove(r.Elem())
				hc.quitMap[t.key] <- true
				delete(hc.realServers, t.key)
				delete(hc.quitMap, t.key)
			}
		} else {
			break
		}
	}
}

func (hc *HealthChecker) checker(rs api.RealServer) (quit chan bool) {
	quit = make(chan bool)
	status := true
	go func() {
		for {
			select {
			case <-quit:
				return
			case <-time.After(5 * time.Second):
				if hc.check(rs) != status {
					hc.Statuses <- api.Status{Key: rs.String(), Status: !status}
					status = !status
				}

			}
		}
	}()
	return
}

func (hc *HealthChecker) check(rs api.RealServer) (status bool) {
	resp, err := hc.netClient.Get(rs.CheckURL())
	if err != nil {
		return
	}
	defer resp.Body.Close()
	if _, err = io.Copy(ioutil.Discard, resp.Body); err != nil {
		return
	}

	if resp.StatusCode < 400 {
		return true
	}
	return
}
